package co.phong.activapaquetesstelcel;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onPaqueteClick(View caller) {

        final Button button = (Button)caller;

        AlertDialog.Builder dialogueBuilder = new AlertDialog.Builder(MainActivity.this);

        dialogueBuilder.setTitle(R.string.pregunta_activar).setMessage(button.getText());

        AlertDialog dialogue = dialogueBuilder.create();

        dialogue.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.activar), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


                String[] idParts = button.getResources().getResourceName(button.getId()).split("_");
                String buttonId = idParts[idParts.length-1];

                sendPackageSMS(buttonId, button.getText().toString());

            }
        });
        dialogue.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancelar), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        dialogue.show();

    }

    void sendPackageSMS(String whichPackage, String packageDescription) {

        SmsManager smsManager = SmsManager.getDefault();

        smsManager.sendTextMessage(getString(R.string.numero_activacion), null, whichPackage, null, null);

        AlertDialog.Builder dialogueBuilder = new AlertDialog.Builder(MainActivity.this);

        dialogueBuilder.setTitle(R.string.se_activo_paquete).setMessage(packageDescription + " (" + whichPackage + ")");

        AlertDialog dialogue = dialogueBuilder.create();
        dialogue.show();
    }


}
